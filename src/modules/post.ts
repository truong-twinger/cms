import { EStatusDoc, TQueryParamsGetData } from '@src/configs/interface.config'

import { TFile } from './media'
import { TTaxonomy } from './taxonomy'
import { TUser } from './user'

export type TPost = {
  _id: string
  name: string
  nameSort: string
  slug: string
  content: string
  excerpt: string
  thumbnail: TFile
  taxonomies: TTaxonomy[]
  scheduleAt: Date
  author: TUser
  editedBy: string
  status: EStatusDoc
  createdAt: Date
  updatedAt: Date
  __v: number
}

export type TCreatePost = {
  name: string
  content?: string
  excerpt?: string
  thumbnailId?: string
  taxonomyIds?: string[]
  scheduleAt?: Date
  status?: EStatusDoc
}

export type TQueryPost = TQueryParamsGetData<{
  status?: EStatusDoc
  'taxonomyIds[]'?: string[]
  'notInIds[]'?: string[]
}>

export type TPatchPost = TCreatePost & {
  name?: string
}
