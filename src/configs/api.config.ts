export const { BASE_URL } = process.env
export const API_TIMEOUT = Number(process.env.API_TIMEOUT) || 30000
